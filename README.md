# Mws magnitude

This SeisComP3 magnitude plugin calculate the moment magnitude from the
waveform spectra using the
[sourcespec](https://gitlab.com/claudiodsf/sourcespec) project.

## Module structure

The Mws magnitude doesn't need an amplitude so it doesn't fit the current
SeisComP3 model with `scamp` and `scmag`.As we wanted this module to be usable
with both `scmag` and `scolv`, we choose to put all the Mws calculation in the
`scamp` module (see https://forum.seiscomp3.org/t/custom-magnitude-plugin/705/5
for a more detailed comparison). The `scmag` just transfers the magnitude
value.

The `scamp` module only accepts C++ plugin and `sourcespec` is written in
Python. So the plugin is a C++ one with a python interpreter to wrap
`sourcespec`.

## Install

```bash
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/path/to/install/dir ..
make install
```

## Config

The configuration is globally the same than the other magnitude plugins.

### global.cfg

As for the Md magnitude, it is necessary to load the plugin.

```ini
# Defines a list of modules loaded at startup.
plugins = dbplugin, mws
```

The following parameters can also be modified:

```ini
module.trunk.global.amplitudes.enableResponses = true
# Equivalent to the pre_p_time, noise_win_length and s_win_length in sourcespec
module.trunk.global.amplitudes.Mws.noiseBegin = -5
module.trunk.global.amplitudes.Mws.noiseEnd = 0
# Time window to retrieve the traces
module.trunk.global.Mws.general.pick_time_before = 60
module.trunk.global.Mws.general.pick_total_time = 180
# Apply a filter before deconvolving data
module.trunk.global.Mws.general.filter = "BW_BP(3, 0.2, 30)"
# Write generated data in this directory
module.trunk.global.Mws.general.outdir = ""
module.trunk.global.Mws.general.PLOT_SHOW = false
module.trunk.global.Mws.general.PLOT_SAVE = false
module.trunk.global.Mws.general.PLOT_SAVE_FORMAT = png
module.trunk.global.Mws.general.database_file = None
module.trunk.global.Mws.general.max_epi_dist = None
module.trunk.global.Mws.initial.vp = 5.5
module.trunk.global.Mws.initial.vs = 3.2
module.trunk.global.Mws.initial.rho = 2500
module.trunk.global.Mws.initial.rps = 0.62
module.trunk.global.Mws.initial.bsd = 1000000
module.trunk.global.Mws.S/N.rmsmin = 0
module.trunk.global.Mws.S/N.vp_tt = None
module.trunk.global.Mws.S/N.vs_tt = None
module.trunk.global.Mws.S/N.sn_min = 0
module.trunk.global.Mws.S/N.gap_max = None
module.trunk.global.Mws.S/N.overlap_max = None
module.trunk.global.Mws.spectrum.wave_type = S
module.trunk.global.Mws.spectrum.time_domain_int = false
module.trunk.global.Mws.spectrum.ignore_vertical = false
module.trunk.global.Mws.spectrum.pre_s_time = 1
module.trunk.global.Mws.spectrum.taper_halfwidth = 0.05
module.trunk.global.Mws.spectrum.spectral_win_length = None
module.trunk.global.Mws.spectrum.bp_freqmin_acc = 1
module.trunk.global.Mws.spectrum.bp_freqmax_acc = 50
module.trunk.global.Mws.spectrum.bp_freqmin_shortp = 1
module.trunk.global.Mws.spectrum.bp_freqmax_shortp = 40
module.trunk.global.Mws.spectrum.bp_freqmin_broadb = 0.5
module.trunk.global.Mws.spectrum.bp_freqmax_broadb = 40
module.trunk.global.Mws.spectrum.freq1_acc = 1
module.trunk.global.Mws.spectrum.freq2_acc = 30
module.trunk.global.Mws.spectrum.freq1_shortp = 1
module.trunk.global.Mws.spectrum.freq2_shortp = 30
module.trunk.global.Mws.spectrum.freq1_broadb = 0.5
module.trunk.global.Mws.spectrum.freq2_broadb = 30
module.trunk.global.Mws.inversion.weighting = noise
module.trunk.global.Mws.inversion.f_weight = 7
module.trunk.global.Mws.inversion.weight = 10
module.trunk.global.Mws.inversion.norm_weight_integral_min = None
module.trunk.global.Mws.inversion.spectral_sn_min = 0
module.trunk.global.Mws.inversion.spectral_sn_freq_min = None
module.trunk.global.Mws.inversion.spectral_sn_freq_max = None
module.trunk.global.Mws.inversion.fc_0 = None
module.trunk.global.Mws.inversion.t_star_0 = 0.045
module.trunk.global.Mws.inversion.invert_t_star_0 = false
module.trunk.global.Mws.inversion.t_star_0_variability = 0.1
module.trunk.global.Mws.inversion.inv_algorithm = LM
module.trunk.global.Mws.inversion.fc_min = None
module.trunk.global.Mws.inversion.fc_max = None
module.trunk.global.Mws.inversion.t_star_min = None
module.trunk.global.Mws.inversion.t_star_max = None
module.trunk.global.Mws.inversion.Qo_min = None
module.trunk.global.Mws.inversion.Qo_max = None
module.trunk.global.Mws.inversion.max_freq_Er = None
module.trunk.global.Mws.post_processing.min_corner_freq = 0.5
module.trunk.global.Mws.post_processing.max_corner_freq = 40
```

So most of the sourcespec parameters can be modified but some are ignored:

```
DEBUG
trace_format
ignore_stations
use_stations
pickle_catalog
pickle_classpath
dataless
paz
traceids
correct_instrumental_response
pre_filt
NLL_model_dir
NLL_time_dir
# Theoretical picks come from seiscomp so no need of these 2 parameters
p_arrival_tolerance
s_arrival_tolerance
pre_p_time
noise_win_length
clip_tolerance
clip_nmax
s_win_length
residuals_filepath
compute_local_magnitude
a
b
c
plot_spectra_no_attenuation
plot_spectra_no_fc
plot_spectra_maxrows
plot_traces_maxrows
plot_station_map
```

In the same way, only the the command line parameter `outdir` has been kept.

### scamp.cfg

Set the `scamp` config to calculate Mws amplitudes.

```ini
# Definition of magnitude types for which amplitudes are to be calculated.
amplitudes = Mws
```

### scmag.cfg

Set the `scmag` config to calculate Mws magnitudes.

```ini
# Definition of magnitude types to be calculated from amplitudes.
magnitudes = Mws
```

## Noise and signal windows in scolv

By recalculating a magnitude in scolv, a noise window and a signal window can
be selected manually with the mouse. These 2 windows must have the same size
for the Mws calculation. Therefore, changing one of the two windows will also
change the size of the other window.

## Known issues

### Stations with 1 component

The magnitude is only calculated with 3 component stations. Some components can
be discarded by `sourcespec` but SeisComP3 needs 3 components otherwise the
`AmplitudeProcessor` is not even called in `scamp`.

See https://forum.seiscomp3.org/t/component-numbers-in-custom-magnitude-plugin/1330.

### Amplitude caching

The Mws magnitude is calculated in an `AmplitudeProcessor` (not a
`MagnitudeProcessor`) but in SeisComP3 the amplitudes are shared between the
different origins of an event. It means that the calculation of the magnitude
are also shared between the origins. It doesn't matter if you only compute
magnitudes on the preferred origin of old events but for the real time
processing it can be really annoying. The `StationMagnitude` will not be
recalculated for a new origin if another origin of the same event has already a
`StationMagnitude` for the same station.

See : https://forum.seiscomp3.org/t/custom-magnitude-plugin/705/6

### Several PAZ stages

A priori only the first PAZ stage is used in SeisComP3.

### Generated amplitudes in scolv while loading traces

In `scolv`, the magnitudes are calculated while loading traces. This
significantly increases the loading time but it seems quite difficult to fix
that.
