#define SEISCOMP_COMPONENT AmplitudeMws

#include "amplitude_mws.h"

#include <boost/python/numpy.hpp>
#include <numpy/ndarrayobject.h>
#include <seiscomp3/client/application.h>
#include <seiscomp3/datamodel/event.h>
#include <seiscomp3/logging/log.h>
#include <seiscomp3/math/filter/butterworth.h>
#include <seiscomp3/math/filter/seismometers.h>
#include <seiscomp3/math/restitution/fft.h>
#include <seiscomp3/math/mean.h>

using namespace std;
using namespace Seiscomp;
using namespace Seiscomp::Config;
using namespace Seiscomp::Core;
using namespace Seiscomp::DataModel;
using namespace Seiscomp::Processing;
namespace py = boost::python;
namespace np = boost::python::numpy;


REGISTER_AMPLITUDEPROCESSOR(AmplitudeProcessor_Mws, AMP_TYPE);


bool AmplitudeProcessor_Mwsi::deconvolveData(Response *resp,
                                            DoubleArray &data,
                                            int numberOfIntegrations) {
    if ( numberOfIntegrations < -1 )
        return false;

    Math::Restitution::FFT::TransferFunctionPtr tf =
        resp->getTransferFunction(numberOfIntegrations < 0 ? 0 : numberOfIntegrations);

    if ( tf == NULL )
        return false;

    // Remove the gain since it isn't done in the tranformFFT function.
    if ( _streamConfig[_usedComponent].gain != 0.0 ) {
        _streamConfig[_usedComponent].applyGain(data);
    }
    else {
        setStatus(MissingGain, 0.0);
        return false;
    }

    // Remove linear trend
    double m,n;
    Math::Statistics::computeLinearTrend(data.size(), data.typedData(), m, n);
    Math::Statistics::detrend(data.size(), data.typedData(), m, n);

    return Math::Restitution::transformFFT(
        data.size(),
        data.typedData(),
        _stream.fsamp, tf.get(),
        _config.respTaper,
        _config.respMinFreq,
        _config.respMaxFreq);
}

AmplitudeProcessor_Mws::AmplitudeProcessor_Mws()
        : Processing::AmplitudeProcessor(AMP_TYPE) {
    setUsedComponent(Any);

    _ampZ.setUsedComponent(Vertical);
    _ampN.setUsedComponent(FirstHorizontal);
    _ampE.setUsedComponent(SecondHorizontal);

    _ampZ.setPublishFunction(
        boost::bind(&AmplitudeProcessor_Mws::newAmplitude, this, _1, _2));
    _ampE.setPublishFunction(
        boost::bind(&AmplitudeProcessor_Mws::newAmplitude, this, _1, _2));
    _ampN.setPublishFunction(
        boost::bind(&AmplitudeProcessor_Mws::newAmplitude, this, _1, _2));

    // Initialize Python interpreter
    if (!Py_IsInitialized()) {
        Py_Initialize();
        np::initialize();

        try {
            _verbosity = SCCoreApp->configGetInt("logging.level");
        } catch (const OptionNotFoundException&) {}

        try {
            py::object main_module = py::import("__main__");
            _pyNs = main_module.attr("__dict__");

            py::exec("import gc", _pyNs);
            py::exec("from obspy.core.event import Arrival, Origin, Pick, "
                        "ResourceIdentifier, WaveformStreamID", _pyNs);
            py::exec("from mws_wrapper import MagnitudeProcessor", _pyNs);
            py::exec("from sourcespec.config import Config", _pyNs);
            py::exec("from sourcespec.ssp_version import get_git_version", _pyNs);
        }
        catch (const py::error_already_set&) {
            string message = handle_pyerror();
            SEISCOMP_DEBUG(message.c_str());
        }
    }

    try {
        py::object main_module = py::import("__main__");
        _pyNs = main_module.attr("__dict__");
    }
    catch (const py::error_already_set&) {
        string message = handle_pyerror();
        SEISCOMP_DEBUG(message.c_str());
    }
}

string AmplitudeProcessor_Mws::handle_pyerror() {
    PyObject *exc, *val, *tb;
    py::object formatted_list, formatted;
    PyErr_Fetch(&exc, &val, &tb);

    py::handle<> hexc(exc), hval(py::allow_null(val)), htb(py::allow_null(tb));
    py::object traceback(py::import("traceback"));

    if (!tb) {
        py::object format_exception_only(
            traceback.attr("format_exception_only")
        );
        formatted_list = format_exception_only(hexc, hval);
    } else {
        py::object format_exception(traceback.attr("format_exception"));
        formatted_list = format_exception(hexc, hval, htb);
    }
    formatted = py::str("\n").join(formatted_list);
    return py::extract<std::string>(formatted);
}


void AmplitudeProcessor_Mws::getPyString(
        const Settings& settings,
        const string& group,
        const string& param,
        const string& defaultValue) {
    try {
        _pyNs["config"][param] =
            settings.getString(AMP_TYPE "." + group + "." + param);
    }
    catch (const OptionNotFoundException&) {
        py::exec(("config." + param + " = " + defaultValue).c_str(), _pyNs);
    }
}


void AmplitudeProcessor_Mws::getPyDouble(
        const Settings& settings,
        const string& group,
        const string& param,
        const string& defaultValue) {
    try {
        _pyNs["config"][param] =
            settings.getDouble(AMP_TYPE "." + group + "." + param);
    }
    catch (const OptionNotFoundException&) {
        py::exec(("config." + param + " = " + defaultValue).c_str(), _pyNs);
    }
}


void AmplitudeProcessor_Mws::getPyBool(
        const Settings& settings,
        const string& group,
        const string& param,
        const string& defaultValue) {
    try {
        _pyNs["config"][param] =
            settings.getBool(AMP_TYPE "." + group + "." + param);
    }
    catch (const OptionNotFoundException&) {
        py::exec(("config." + param + " = " + defaultValue).c_str(), _pyNs);
    }
}


void AmplitudeProcessor_Mws::setupPython(const Settings& settings) {
    _pyNs["config"] = _pyNs["Config"]();

    // General parameters
    getPyBool(settings, "general", "PLOT_SHOW", "False");
    getPyBool(settings, "general", "PLOT_SAVE", "False");
    getPyString(settings, "general", "PLOT_SAVE_FORMAT", "'png'");
    getPyString(settings, "general", "outdir", "''");
    getPyString(settings, "general", "database_file", "None");
    getPyDouble(settings, "general", "max_epi_dist", "None");

    // Initial parameters
    getPyDouble(settings, "initial", "vp", "5.5");
    getPyDouble(settings, "initial", "vs", "3.2");
    getPyDouble(settings, "initial", "rho", "2500");
    getPyDouble(settings, "initial", "rps", "0.62");
    getPyDouble(settings, "initial", "bsd", "10e6");

    // S/N parameters
    getPyDouble(settings, "S/N", "rmsmin", "0");
    getPyDouble(settings, "S/N", "vp_tt", "None");
    getPyDouble(settings, "S/N", "vs_tt", "None");
    getPyDouble(settings, "S/N", "sn_min", "0");
    getPyDouble(settings, "S/N", "gap_max", "None");
    getPyDouble(settings, "S/N", "overlap_max", "None");

    // Spectrum parameters
    getPyString(settings, "spectrum", "wave_type", "'S'");
    getPyBool(settings, "spectrum", "time_domain_int", "False");
    getPyBool(settings, "spectrum", "ignore_vertical", "False");
    getPyDouble(settings, "spectrum", "pre_s_time", "1");
    getPyDouble(settings, "spectrum", "taper_halfwidth", "0.05");
    getPyDouble(settings, "spectrum", "spectral_win_length", "None");
    getPyDouble(settings, "spectrum", "bp_freqmin_acc", "1");
    getPyDouble(settings, "spectrum", "bp_freqmax_acc", "50");
    getPyDouble(settings, "spectrum", "bp_freqmin_shortp", "1");
    getPyDouble(settings, "spectrum", "bp_freqmax_shortp", "40");
    getPyDouble(settings, "spectrum", "bp_freqmin_broadb", "0.5");
    getPyDouble(settings, "spectrum", "bp_freqmax_broadb", "40");
    getPyDouble(settings, "spectrum", "freq1_acc", "1");
    getPyDouble(settings, "spectrum", "freq2_acc", "30");
    getPyDouble(settings, "spectrum", "freq1_shortp", "1");
    getPyDouble(settings, "spectrum", "freq2_shortp", "30");
    getPyDouble(settings, "spectrum", "freq1_broadb", "0.5");
    getPyDouble(settings, "spectrum", "freq2_broadb", "30");

    // Inversion parameters
    getPyString(settings, "inversion", "weighting", "'noise'");
    getPyDouble(settings, "inversion", "f_weight", "7");
    getPyDouble(settings, "inversion", "weight", "10");
    getPyDouble(settings, "inversion", "norm_weight_integral_min", "None");
    getPyDouble(settings, "inversion", "spectral_sn_min", "0");
    getPyDouble(settings, "inversion", "spectral_sn_freq_min", "None");
    getPyDouble(settings, "inversion", "spectral_sn_freq_max", "None");
    getPyDouble(settings, "inversion", "fc_0", "None");
    getPyDouble(settings, "inversion", "t_star_0", "0.045");
    getPyBool(settings, "inversion", "invert_t_star_0", "False");
    getPyDouble(settings, "inversion", "t_star_0_variability", "0.1");
    getPyString(settings, "inversion", "inv_algorithm", "'LM'");
    getPyDouble(settings, "inversion", "fc_min", "None");
    getPyDouble(settings, "inversion", "fc_max", "None");
    getPyDouble(settings, "inversion", "t_star_min", "None");
    getPyDouble(settings, "inversion", "t_star_max", "None");
    getPyDouble(settings, "inversion", "Qo_min", "None");
    getPyDouble(settings, "inversion", "Qo_max", "None");
    getPyDouble(settings, "inversion", "max_freq_Er", "None");

    // Post-processing parameters
    getPyDouble(settings, "post_processing", "min_corner_freq", "0.5");
    getPyDouble(settings, "post_processing", "max_corner_freq", "40");

    py::exec((_processorName + ".setup(config)").c_str(), _pyNs);
}


void AmplitudeProcessor_Mws::addTrace(Component component) {
    const AmplitudeProcessor_Mwsi* processor =
        (AmplitudeProcessor_Mwsi*) componentProcessor(component);
    const DoubleArray* data = processedData(component);

    npy_intp size = data->size();
    double * data_array = size ? const_cast<double *>(data->typedData())
        : static_cast<double *>(NULL);

    // Convert C++ array to numpy array
    np::ndarray numpyData = np::from_data(
        data_array,
        np::dtype::get_builtin<double>(),
        py::make_tuple(size),
        py::make_tuple(sizeof(double)),
        py::object()
    );

    _pyNs[_processorName].attr("add_trace")(
        numpyData.copy(),
        processor->samplingFrequency(),
        streamConfig(component).code(),
        (double) processor->_stream.dataTimeWindow.startTime()
    );
}

double AmplitudeProcessor_Mws::getOriginPickTime(const Origin* origin,
                                                 const string& phaseCode,
                                                 const bool firstLetterOnly) {
    double earlierPickTime = 0;

    for ( size_t i = 0; i < origin->arrivalCount(); ++i ) {
        Arrival* arrival = origin->arrival(i);

        if (arrival->weight() == 0) {
            continue;
        }

        string pickID = arrival->pickID();
        PickPtr pick = Pick::Cast(
            SCCoreApp->query()->getObject(Pick::TypeInfo(), pickID)
        );

        if ( !pick ) {
            SEISCOMP_WARNING("Pick with id '%s' not found", pickID.c_str());
            continue;
        }

        if ( _networkCode != pick->waveformID().networkCode()) {
            continue;
        }

        if ( _stationCode != pick->waveformID().stationCode()) {
            continue;
        }

        if ( _locationCode != pick->waveformID().locationCode()) {
            continue;
        }

        if ( firstLetterOnly ) {
            if ( pick->phaseHint().code()[0] != phaseCode[0] ) {
                continue;
            }
        }
        else {
            if (pick->phaseHint().code() != phaseCode ) {
                continue;
            }
        }

        double pickTime = (double) pick->time().value();

        if ( earlierPickTime == 0 || pickTime < earlierPickTime ) {
            earlierPickTime = pickTime;
        }
    }

    if (earlierPickTime == 0) {
        throw NoPhaseError();
    }

    return earlierPickTime;
}

double AmplitudeProcessor_Mws::getTheoreticalPickTime(
        const Origin* origin,
        const Station* station,
        const string& phaseCode) {
	TravelTimeTableInterfacePtr _ttTable =
        TravelTimeTableInterfaceFactory::Create(origin->methodID().c_str());

	if ( !_ttTable ) {
        SEISCOMP_ERROR("Error creating travel time table backend %s",
                       origin->methodID().c_str());
        throw NoPhaseError();
	}
	else if ( !_ttTable->setModel(origin->earthModelID()) ) {
        SEISCOMP_ERROR("Failed to set table %s",
                       origin->earthModelID().c_str());
        throw NoPhaseError();
	}

    TravelTime tt = _ttTable->compute(
        phaseCode.c_str(),
        origin->latitude(),
        origin->longitude(),
        origin->depth(),
        station->latitude(),
        station->longitude(),
        station->elevation()
    );

    return tt.time + (double) origin->time().value();
 }

double AmplitudeProcessor_Mws::getPPickTime(
        const Origin* origin,
        const Station* station) {

    try {
        return getOriginPickTime(origin, "P", true);
    }
    catch (const NoPhaseError&) {
        SEISCOMP_DEBUG("No origin phase P found for %s", station->code().c_str());
    }

    try {
        return getTheoreticalPickTime(origin, station, "P");
    }
    catch (const NoPhaseError&) {
        SEISCOMP_DEBUG("No theoretical phase P found for %s", station->code().c_str());
    }

    throw NoPhaseError();
}

double AmplitudeProcessor_Mws::getSPickTime(
        const Origin* origin,
        const Station* station) {
    try {
        return getOriginPickTime(origin, "Sg", false);
    }
    catch (const NoPhaseError&) {
        SEISCOMP_DEBUG("No origin phase Sg found for %s", station->code().c_str());
    }

    try {
        return getOriginPickTime(origin, "S", false);
    }
    catch (const NoPhaseError&) {
        SEISCOMP_DEBUG("No origin phase S found for %s", station->code().c_str());
    }

    try {
        return getTheoreticalPickTime(origin, station, "Sg");
    }
    catch (const NoPhaseError&) {
        SEISCOMP_DEBUG("No theoretical phase Sg found for %s", station->code().c_str());
    }

    try {
        return getTheoreticalPickTime(origin, station, "S");
    }
    catch (const NoPhaseError&) {
        SEISCOMP_DEBUG("No theoretical phase S found for %s", station->code().c_str());
    }

    throw NoPhaseError();
}

void AmplitudeProcessor_Mws::addPicks(const Origin* origin,
                                      const Station* station) {
    try {
        double pPickTime = getPPickTime(origin, station);
        _pyNs[_processorName].attr("set_pick")("P", pPickTime);
    }
    catch (const NoPhaseError&) {
        SEISCOMP_WARNING("No phase P found for %s", station->code().c_str());
    }

    try {
        double sPickTime = getSPickTime(origin, station);
        _pyNs[_processorName].attr("set_pick")("S", sPickTime);
    }
    catch (const NoPhaseError&) {
        SEISCOMP_WARNING("No phase S or Sg found for %s", station->code().c_str());
    }
}

void AmplitudeProcessor_Mws::setConfig(const Config &config) {
    AmplitudeProcessor::setConfig(config);
    _ampZ.setConfig(config);
    _ampN.setConfig(config);
    _ampE.setConfig(config);
}


const AmplitudeProcessor *AmplitudeProcessor_Mws::componentProcessor(
        Component comp) const {
    switch ( comp ) {
        case VerticalComponent:
            return &_ampZ;
        case FirstHorizontalComponent:
            return &_ampN;
        case SecondHorizontalComponent:
            return &_ampE;
        default:
            break;
    }

    return NULL;
}


const DoubleArray *AmplitudeProcessor_Mws::processedData(Component comp) const {
    switch ( comp ) {
        case VerticalComponent:
            return _ampZ.processedData(comp);
        case FirstHorizontalComponent:
            return _ampN.processedData(comp);
        case SecondHorizontalComponent:
            return _ampE.processedData(comp);
        default:
            break;
    }

    return NULL;
}


void AmplitudeProcessor_Mws::reprocess(OPT(double) searchBegin,
                                       OPT(double) searchEnd) {
    setStatus(WaitingForData, 0);

    _ampZ.reprocess(searchBegin, searchEnd);
    _ampN.reprocess(searchBegin, searchEnd);
    _ampE.reprocess(searchBegin, searchEnd);

    if ( !isFinished() ) {
        if ( _ampZ.status() > Finished )
            setStatus(_ampZ.status(), _ampZ.statusValue());
        else if ( _ampN.status() > Finished )
            setStatus(_ampN.status(), _ampN.statusValue());
        else
            setStatus(_ampE.status(), _ampE.statusValue());
    }
}


void AmplitudeProcessor_Mws::reset() {
    AmplitudeProcessor::reset();

    _ampZReady = false;
    _ampNReady = false;
    _ampEReady = false;

    _ampZ.reset();
    _ampN.reset();
    _ampE.reset();
}


void AmplitudeProcessor_Mws::computeTimeWindow() {
    const Origin* origin = _environment.hypocenter;

    // Retrieve station to get theoretical pick
    Station* station = SCCoreApp->query()->getStation(
        _networkCode,
        _stationCode,
        _trigger
    );

    double sPickTime = 0;

    try {
        sPickTime = getSPickTime(origin, station);
    }
    catch (const NoPhaseError&) {
        SEISCOMP_WARNING(
            "The time window will be incorrect since no phase has been retrieved"
        );
    }

    // Update noise and signal time windows
    try {
        _pyNs[_processorName].attr("update_windows")(
            _config.noiseBegin,
            _config.noiseEnd,
            _config.signalBegin,
            _config.signalEnd,
            (double) _trigger,
            sPickTime
        );

        double prePTime = py::extract<double>(
            _pyNs[_processorName].attr("config").attr("pre_p_time")
        );
        double preSTime = py::extract<double>(
            _pyNs[_processorName].attr("config").attr("pre_s_time")
        );
        double noiseWinLength = py::extract<double>(
            _pyNs[_processorName].attr("config").attr("noise_win_length")
        );
        double signalWinLength = py::extract<double>(
            _pyNs[_processorName].attr("config").attr("s_win_length")
        );

        _config.noiseBegin = -prePTime;
        _config.noiseEnd = _config.noiseBegin + noiseWinLength;
        _config.signalBegin = sPickTime - ((double) _trigger) - preSTime;
        _config.signalEnd = _config.signalBegin + signalWinLength;
    }
    catch (const py::error_already_set&) {
        string message = handle_pyerror();
        SEISCOMP_DEBUG(message.c_str());
    }

    // Update global time window
    Core::Time startTime = _trigger - TimeSpan(_pickTimeBefore);
    Core::Time endTime = _trigger + TimeSpan(_pickTotalTime);

    Core::TimeWindow timeWindow = Core::TimeWindow(startTime, endTime);

    setTimeWindow(timeWindow);
    _ampZ.setTimeWindow(timeWindow);
    _ampN.setTimeWindow(timeWindow);
    _ampE.setTimeWindow(timeWindow);
}

bool AmplitudeProcessor_Mws::setup(const Settings& settings) {
    // Copy the stream configurations (gain, orientation, responses, ...) to
    // the horizontal processors
    _ampZ.streamConfig(VerticalComponent) = streamConfig(VerticalComponent);
    _ampN.streamConfig(FirstHorizontalComponent) =
        streamConfig(FirstHorizontalComponent);
    _ampE.streamConfig(SecondHorizontalComponent) =
        streamConfig(SecondHorizontalComponent);

    if ( !AmplitudeProcessor::setup(settings) ) return false;

    // Setup each component
    if ( !_ampZ.setup(settings)
            || !_ampN.setup(settings)
            || !_ampE.setup(settings) ) {
        return false;
    }

    // Propagate _enableResponses. The magnitude type can be in the parameter
    // name and not read by the internal processor. Moreover, this parameter
    // isn't store in the config object.
    _ampZ._enableResponses = _enableResponses;
    _ampN._enableResponses = _enableResponses;
    _ampE._enableResponses = _enableResponses;

    _ampZ.setConfig(config());
    _ampN.setConfig(config());
    _ampE.setConfig(config());

    _networkCode = settings.networkCode;
    _stationCode = settings.stationCode;
    _locationCode = settings.locationCode;
    _channelCode = settings.channelCode;

    settings.getValue(_pickTimeBefore, AMP_TYPE ".general.pick_time_before");
    settings.getValue(_pickTotalTime, AMP_TYPE ".general.pick_total_time");
    settings.getValue(_filterStr, AMP_TYPE ".general.filter");

    try {
        _processorName = "processor_"
            + _networkCode + "_"
            + _stationCode + "_"
            + _locationCode + "_"
            + _channelCode;

        _pyNs[_processorName] = _pyNs["MagnitudeProcessor"](_verbosity);
        setupPython(settings);
    }
    catch (const py::error_already_set&) {
        string message = handle_pyerror();
        SEISCOMP_DEBUG(message.c_str());
    }

    // Set filter to use
    Filter *filter = Filter::Create(_filterStr);
    if ( filter == NULL ) {
        SEISCOMP_INFO("Can't create filter, signal not filtered.");
        return true;
    }

    _ampZ.setFilter(filter);
    _ampN.setFilter(filter->clone());
    _ampE.setFilter(filter->clone());

    return true;
}


void AmplitudeProcessor_Mws::setTrigger(const Core::Time& trigger) {
    AmplitudeProcessor::setTrigger(trigger);
    _ampZ.setTrigger(trigger);
    _ampN.setTrigger(trigger);
    _ampE.setTrigger(trigger);
}


double AmplitudeProcessor_Mws::timeWindowLength(double distance_deg) const {
    double endZ = _ampZ.timeWindowLength(distance_deg);
    double endN = _ampN.timeWindowLength(distance_deg);
    double endE = _ampE.timeWindowLength(distance_deg);

    _ampZ.setSignalEnd(endZ);
    _ampN.setSignalEnd(endN);
    _ampE.setSignalEnd(endE);

    // So ugly…
    return std::max(endZ, std::max(endN, endE));
}


bool AmplitudeProcessor_Mws::feed(const Record *record) {
    // All processors finished already?
    if ( _ampZ.isFinished() && _ampE.isFinished() && _ampN.isFinished() ) {
        return false;
    }

    // Did an error occur?
    if ( status() > WaveformProcessor::Finished ) {
        return false;
    }

    if ( record->channelCode() == _streamConfig[VerticalComponent].code() ) {
        if ( !_ampZ.isFinished() ) {
            _ampZ.feed(record);
        }
    }
    else if ( record->channelCode() ==
            _streamConfig[FirstHorizontalComponent].code() ) {
        if ( !_ampN.isFinished() ) {
            _ampN.feed(record);
        }
    }
    else if ( record->channelCode() ==
            _streamConfig[SecondHorizontalComponent].code() ) {
        if ( !_ampE.isFinished() ) {
            _ampE.feed(record);
        }
    }

    updateStatus();

    return true;
}

void AmplitudeProcessor_Mws::updateStatus() {
    if ( !_ampZ.isFinished() || !_ampN.isFinished() || !_ampE.isFinished() ) {
        setStatus(
            WaveformProcessor::InProgress,
            (_ampZ.statusValue() + _ampN.statusValue() + _ampE.statusValue()) / 3
        );
    }
    else if ( _ampZ.status() == Finished || _ampN.status() == Finished || _ampE.status() == Finished ) {
        setStatus(WaveformProcessor::Finished, 100);
    }
    else if ( _ampZ.status() > Finished ) {
        setStatus(_ampZ.status(), _ampZ.statusValue());
    }
    else if ( _ampN.status() > Finished ) {
        setStatus(_ampN.status(), _ampN.statusValue());
    }
    else if ( _ampE.status() > Finished ) {
        setStatus(_ampE.status(), _ampE.statusValue());
    }
}

void AmplitudeProcessor_Mws::newAmplitude(
        const AmplitudeProcessor *proc,
        const AmplitudeProcessor::Result &res) {
    if ( isFinished() ) return;

    if ( !_ampZ.isFinished() || !_ampN.isFinished() || !_ampE.isFinished() ) {
        return;
    }

    const Origin* origin = _environment.hypocenter;

    // Retrieve event to create the log folder
    const Event* event = SCCoreApp->query()->getEvent(origin->publicID());
    string eventId;
    if (event != NULL) {
        eventId = event->publicID();
    }

    // Retrieve station to get lat, long, depth
    const Station* station = SCCoreApp->query()->getStation(
        _networkCode,
        _stationCode,
        _trigger
    );

    try {
        _pyNs[_processorName].attr("reset")();

        _pyNs[_processorName].attr("create_log_directory")(
            eventId,
            origin->publicID()
        );

        // Create a new logger to write log in the log directory
        // previously created.
        _pyNs[_processorName].attr("init_logger")();

        _pyNs[_processorName].attr("set_station_code")(
            _networkCode,
            _stationCode,
            _locationCode,
            _channelCode
        );

        _pyNs[_processorName].attr("set_station_coordinates")(
            station->latitude(),
            station->longitude(),
            station->elevation()
        );

        _pyNs[_processorName].attr("set_origin")(
            origin->publicID(),
            (double) origin->time().value(),
            origin->latitude().value(),
            origin->longitude().value(),
            origin->depth().value()
        );

        if (_ampZ.status() == Finished) {
            addTrace(VerticalComponent);
        }
        if (_ampN.status() == Finished) {
            addTrace(FirstHorizontalComponent);
        }
        if (_ampE.status() == Finished) {
            addTrace(SecondHorizontalComponent);
        }

        addPicks(origin, station);

        SEISCOMP_INFO("Calculate magnitude %s for %s.%s.%s.%s",
            AMP_TYPE,
            _networkCode.c_str(),
            _stationCode.c_str(),
            _locationCode.c_str(),
            _channelCode.c_str());

        _pyNs[_processorName].attr("process")();

        double value = py::extract<double>(
            _pyNs[_processorName].attr("result").attr("mws")[0]
        );

        double uncertainty = py::extract<double>(
            _pyNs[_processorName].attr("result").attr("mw_uncertainties")[0]
        );

        if ( isnan(value) ) {
            SEISCOMP_WARNING("No magnitude %s calculated for %s.%s.%s.%s",
                AMP_TYPE,
                _networkCode.c_str(),
                _stationCode.c_str(),
                _locationCode.c_str(),
                _channelCode.c_str());
        }
        else {
            SEISCOMP_INFO("Magnitude %s for %s.%s.%s.%s: %f",
                AMP_TYPE,
                _networkCode.c_str(),
                _stationCode.c_str(),
                _locationCode.c_str(),
                _channelCode.c_str(),
                value);

            setStatus(Finished, 100.);
            Result newRes;
            newRes.record = res.record;
            newRes.amplitude.value = value;
            newRes.amplitude.lowerUncertainty = uncertainty;
            newRes.amplitude.upperUncertainty = uncertainty;
            newRes.time.reference = _trigger;
            newRes.component = Horizontal;
            newRes.period = -1;
            newRes.snr = -1;
            emitAmplitude(newRes);
        }
    }
    catch (const py::error_already_set&) {
        string message = handle_pyerror();
        SEISCOMP_DEBUG(message.c_str());

        SEISCOMP_INFO("Can't calculate magnitude %s for %s.%s.%s.%s",
            AMP_TYPE,
            _networkCode.c_str(),
            _stationCode.c_str(),
            _locationCode.c_str(),
            _channelCode.c_str());
    }

    try {
        // Free some memory
        py::exec((_processorName + " = None").c_str(), _pyNs);
        py::exec("config = None", _pyNs);
        py::exec("gc.collect()", _pyNs);
    }
    catch (const py::error_already_set&) {
        string message = handle_pyerror();
        SEISCOMP_DEBUG(message.c_str());
    }
}
