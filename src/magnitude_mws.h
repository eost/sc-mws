#ifndef __MWS_PLUGIN_H__
#define __MWS_PLUGIN_H__

#include "amplitude_mws.h"

#include <seiscomp3/processing/magnitudeprocessor.h>


#define MAG_TYPE "Mws"


using namespace std;
using namespace Seiscomp::DataModel;
using namespace Seiscomp::Processing;


class MagnitudeProcessor_Mws : public MagnitudeProcessor {

    public:
        MagnitudeProcessor_Mws() : MagnitudeProcessor(MAG_TYPE){};

        inline std::string amplitudeType() const { return AMP_TYPE; };

        MagnitudeProcessor::Status computeMagnitude(
            double amplitudeValue,
            const std::string &unit,
            double period,      // in seconds
            double snr,
            double delta,       // in degrees
            double depth,       // in kilometers
            const Origin * hypocenter,
            const SensorLocation * receiver,
            const Amplitude * amplitude,
            double &value);

		void finalizeMagnitude(DataModel::StationMagnitude *magnitude) const;
};

#endif
