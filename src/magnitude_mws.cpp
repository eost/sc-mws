#define SEISCOMP_COMPONENT Mws

#include "magnitude_mws.h"


ADD_SC_PLUGIN(
    MAG_TYPE,
    MAG_TYPE,
    0, 0, 1
);


REGISTER_MAGNITUDEPROCESSOR(MagnitudeProcessor_Mws, MAG_TYPE);


MagnitudeProcessor::Status MagnitudeProcessor_Mws::computeMagnitude(
        double amplitudeValue,
        const std::string &unit,
        double period,      // in seconds
        double snr,
        double delta,       // in degrees
        double depth,       // in kilometers
        const Origin * hypocenter,
        const SensorLocation * receiver,
        const Amplitude * amplitude,
        double &value)
{
    value = amplitudeValue;
    return OK;
}

void MagnitudeProcessor_Mws::finalizeMagnitude(
        DataModel::StationMagnitude *magnitude) const {
	// Nothing
}