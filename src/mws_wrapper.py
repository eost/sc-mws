#! /usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import math
import os
import signal

from sourcespec.ssp_build_spectra import build_spectra
from sourcespec.config import Config
from sourcespec.ssp_inversion import spectral_inversion
from sourcespec.ssp_output import write_output
from sourcespec.ssp_plot_spectra import plot_spectra
from sourcespec.ssp_plot_traces import plot_traces
from sourcespec.ssp_process_traces import (_add_hypo_dist_and_arrivals,
                                           _check_sn_ratio,
                                           _merge_stream,
                                           filter_trace)
from sourcespec.ssp_read_traces import (_add_instrtype, _add_hypocenter,
                                        _add_picks)
from sourcespec.ssp_residuals import spectral_residuals
from sourcespec.ssp_setup import init_plotting

from obspy.core import Stream, Trace, UTCDateTime
from obspy.core.event import Origin
from obspy.core.util import AttribDict
import numpy as np


logger = logging.getLogger()

# Allow to kill the C++ module and the Python interpreter.
signal.signal(signal.SIGINT, signal.SIG_DFL)


def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


class MagnitudeResult(object):
    """
    Store the Mws magnitude result in this object to help
    the C++/Python wrapping along.
    """
    def __init__(self, sourcepar, sourcepar_err):
        self.magnitude = None
        self.magnitude_uncertainty = None
        self.codes = []
        self.mws = []
        self.mw_uncertainties = []
        self.fcs = []
        self.fc_uncertainties = []
        self.t_stars = []
        self.t_star_uncertainties = []

        self.fill(sourcepar, sourcepar_err)

    def fill(self, sourcepar, sourcepar_err):
        """
        Fill the result object from the sourcepar and sourcepar_err
        returned by sourcespec.
        """
        # Add magnitude value and uncertainty
        mws = np.array([v['Mw'] for v in sourcepar.values()])
        self.magnitude = mws.mean()
        self.magnitude_uncertainty = mws.std()

        # Add station_magnitude data
        for string_id in sourcepar.keys():
            network, station, location, channel = \
                string_id.split()[0].split('.')

            self.codes.append((
                network.encode('utf-8'),
                station.encode('utf-8'),
                location.encode('utf-8'),
                channel.encode('utf-8'),
            ))
            self.mws.append(sourcepar[string_id]['Mw'])
            self.mw_uncertainties.append(sourcepar_err[string_id]['Mw'])
            self.fcs.append(sourcepar[string_id]['fc'])
            self.fc_uncertainties.append(sourcepar_err[string_id]['fc'])
            self.t_stars.append(sourcepar[string_id]['t_star'])
            self.t_star_uncertainties.append(
                sourcepar_err[string_id]['t_star'])


class MagnitudeProcessor(object):
    """
    Calculate a Mws magnitude from an ObsPy Origin.
    """
    def __init__(self, verbosity=0):
        MagnitudeProcessor.set_log_level(verbosity)

        self.config = Config()
        self.network_code = ''
        self.station_code = ''
        self.location_code = ''
        self.channel_code = ''
        self.stream = Stream()
        self.origin = None
        self.custom_P_pick = None
        self.custom_S_pick = None

    def setup(self, config):
        self.config = config
        self.config.options = Config()
        self.config.options.correction = False

        if self.config.PLOT_SAVE_FORMAT not in ['png', 'pdf']:
            logger.warning("'PLOT_SAVE_FORMAT' is set to '%s' but must be "
                           "'png' or 'pdf'."
                           % self.config.PLOT_SAVE_FORMAT)

        self.config.options.outdir = self.config.outdir
        self.config.options.hypo_file = None

        self.config.ignore_stations = None
        self.config.use_stations = None

        self.config.NLL_model_dir = None
        self.config.NLL_time_dir = None

        self.config.p_arrival_tolerance = float('inf')
        self.config.s_arrival_tolerance = float('inf')

        if self.config.wave_type not in ['S', 'SH', 'SV']:
            logger.warning("'wave_type' is set to '%s' but must be 'S', 'SH' "
                           "or 'SV'. Use 'S' instead."
                           % self.config.wave_type)
            self.config.wave_type = 'S'

        if self.config.weighting not in ['noise', 'frequency', 'None']:
            logger.warning("'weighting' is set to '%s' but must be 'noise', "
                           "'frequency' or 'None'. Use 'noise' instead."
                           % self.config.weighting)
            self.config.weighting = 'noise'
        if self.config.weighting == 'None':
            self.config.weighting = None

        if self.config.spectral_sn_freq_min is None or \
                self.config.spectral_sn_freq_max is None:
            self.config.spectral_sn_freq_range = None
        else:
            self.config.spectral_sn_freq_range = [
                self.config.spectral_sn_freq_min,
                self.config.spectral_sn_freq_max,
            ]

        if self.config.inv_algorithm not in ["LM", "TNC", "BH"]:
            logger.warning("'inv_algorithm' is set to '%s' but must be 'LM', "
                           "'TNC', or 'BH'. Use 'LM' instead."
                           % self.config.inv_algorithm)
            self.config.inv_algorithm = 'LM'

        if self.config.fc_min is None or self.config.fc_max is None:
            self.config.fc_min_max = None
        else:
            self.config.fc_min_max = [
                self.config.fc_min,
                self.config.fc_max,
            ]

        if self.config.t_star_min is None or self.config.t_star_max is None:
            self.config.t_star_min_max = None
        else:
            self.config.t_star_min_max = [
                self.config.t_star_min,
                self.config.t_star_max,
            ]

        if self.config.Qo_min is None or self.config.Qo_max is None:
            self.config.Qo_min_max = None
        else:
            self.config.Qo_min_max = [
                self.config.Qo_min,
                self.config.Qo_max,
            ]

        self.config.plot_spectra_no_attenuation = False
        self.config.plot_spectra_no_fc = False
        self.config.plot_spectra_maxrows = 1
        self.config.plot_traces_maxrows = 1

    def update_windows(self, noise_begin, noise_end, signal_begin, signal_end,
                       trigger_time, pick_time):
        relative_pick_time = pick_time - trigger_time

        self.config.pre_p_time = -noise_begin

        try:
            # Size of noise window changed
            if not isclose(noise_end - noise_begin,
                           self.config.noise_win_length):
                self.config.noise_win_length = noise_end - noise_begin
                self.config.s_win_length = self.config.noise_win_length
            # Size of signal window changed
            elif not isclose(signal_end - signal_begin,
                             self.config.s_win_length):
                self.config.s_win_length = signal_end - signal_begin
                self.config.noise_win_length = self.config.s_win_length
        except AttributeError:
            # First time the function is called
            self.config.noise_win_length = noise_end - noise_begin
            self.config.s_win_length = self.config.noise_win_length
        else:
            # Not set the first time to use the config parameter
            self.config.pre_s_time = relative_pick_time - signal_begin

    def reset(self):
        self.stream = Stream()
        self.custom_P_pick = None
        self.custom_S_pick = None

    @staticmethod
    def set_log_level(verbosity):
        levels = [logging.ERROR, logging.WARNING, logging.INFO, logging.DEBUG]
        log_level = levels[min(len(levels) - 1, verbosity)]
        log_format = '%(asctime)s %(name)s: %(levelname)s: %(message)s'
        datefmt = "%Y-%m-%d %H:%M:%S"

        logging.basicConfig(
            level=log_level,
            format=log_format,
            datefmt=datefmt,
        )

    def create_log_directory(self, event_id, origin_id):
        # Create the directory if it does not exist (can crash in sourcespec)
        self.config.options.outdir = os.path.join(
            self.config.outdir,
            (event_id or '').replace('/', '_'),
            origin_id.replace('/', '_'),
        )
        try:
            os.makedirs(self.config.options.outdir)
        except OSError:
            if not os.path.isdir(self.config.options.outdir):
                raise

    def init_logger(self):
        log_format = '%(asctime)s %(name)s: %(levelname)s: %(message)s'
        datefmt = "%Y-%m-%d %H:%M:%S"

        # Add an handler to store logs in a separated file
        log_file = os.path.join(self.config.options.outdir, 'mws.log')
        handler = logging.FileHandler(log_file)
        fmt = logging.Formatter(log_format, datefmt=datefmt)
        handler.setFormatter(fmt)
        # Remove all old handlers
        for hdlr in logger.handlers[:]:
            if isinstance(hdlr, logging.FileHandler):
                logger.removeHandler(hdlr)
        logger.addHandler(handler)

    def set_station_code(self, network_code, station_code, location_code,
                         channel_code):
        self.network_code = network_code
        self.station_code = station_code
        self.location_code = location_code
        self.channel_code = channel_code

    def set_station_coordinates(self, latitude, longitude, elevation):
        self.station_latitude = latitude
        self.station_longitude = longitude
        # Store elevation in m
        self.station_elevation = elevation

    def set_origin(self, resource_id, origin_time, latitude, longitude, depth):
        self.origin = Origin()
        self.origin.resource_id = resource_id
        self.origin.time = origin_time
        self.origin.latitude = latitude
        self.origin.longitude = longitude
        # Store depth in m
        self.origin.depth = depth * 1000

    def add_trace(self, data, sampling_rate, channel_code, start_time):
        """
        Add a new Trace to the Stream from SeisComP3 data.
        """
        trace = Trace(data)
        trace.stats.sampling_rate = sampling_rate
        trace.stats.network = self.network_code
        trace.stats.station = self.station_code
        trace.stats.location = self.location_code
        trace.stats.channel = channel_code
        trace.stats.starttime = start_time

        self.stream.append(trace)

    def set_pick(self, phase, pick_time):
        custom_pick = AttribDict()
        custom_pick.station = self.station_code
        # Sourcespec needs only 1 phase letter
        custom_pick.phase = phase[:1]
        custom_pick.time = UTCDateTime(pick_time)

        logger.debug(
            'Add pick %s (%s)' % (phase, custom_pick.time)
        )

        if custom_pick.phase == 'P':
            self.custom_P_pick = custom_pick
        elif custom_pick.phase == 'S':
            self.custom_S_pick = custom_pick

    def _prepare_stream(self):
        """
        Retrieve all needed waveforms and return a Stream.
        """
        logger.info('Retrieve %s.%s.%s.%s stream' % (
            self.network_code,
            self.station_code,
            self.location_code,
            self.channel_code,
        ))

        coords = AttribDict()
        # Use elevation in km
        coords.elevation = self.station_elevation / 1000
        coords.latitude = self.station_latitude
        coords.longitude = self.station_longitude

        hypo = AttribDict()
        hypo.origin_time = self.origin.time
        hypo.latitude = self.origin.latitude
        hypo.longitude = self.origin.longitude
        hypo.depth = self.origin.depth / 1000
        hypo.vs = self.config.vs
        # `evid` is only used for filename…
        hypo.evid = '%s.%s.%s.%s' % (
            self.network_code,
            self.station_code,
            self.location_code,
            self.channel_code,
        )

        self.config.hypo = hypo

        for trace in self.stream:
            trace.stats.coords = coords
            _add_instrtype(trace)
            _add_hypocenter(trace, hypo)
            _add_picks(trace, [self.custom_P_pick, self.custom_S_pick])

    def _process_traces(self):
        """
        Same function than sourcespec process_traces without filtering
        the traces.
        """
        out_st = Stream()
        for trace_id in sorted({tr.id for tr in self.stream}):
            st_sel = self.stream.select(id=trace_id)

            try:
                _add_hypo_dist_and_arrivals(self.config, st_sel)
                trace = _merge_stream(self.config, st_sel)

                filter_trace(self.config, trace)

                # Check if the trace has significant signal to noise ratio
                _check_sn_ratio(self.config, trace)

                out_st.append(trace)
            except (ValueError, RuntimeError):
                continue

        self.stream = out_st

    def process(self):
        """
        Calculate the station magnitude.

        epicentral_distance: in degrees
        elevation: in meters
        depth: in kilometers
        """
        logger.info("Calculate magnitude for %s.%s.%s.%s" % (
            self.network_code,
            self.station_code,
            self.location_code,
            self.channel_code,
        ))

        # Check that both picks are set
        if self.custom_P_pick is None or self.custom_S_pick is None:
            logger.error("Some picks can not be retrieved")
            return

        # Sourcespec `read_traces` equivalent
        self._prepare_stream()

        # Sourcespec `process_traces` equivalent
        self._process_traces()

        if len(self.stream) == 0:
            logger.info("No trace left in stream")
            return

        # Build spectra (amplitude in magnitude units)
        spec_st, specnoise_st, weight_st =\
            build_spectra(self.config, self.stream, noise_weight=True)

        if len(spec_st) == 0:
            logger.info("No trace left in spectral stream")
            return

        plotter = init_plotting()
        plot_traces(self.config, self.stream, spec_st=spec_st, ncols=2,
                    async_plotter=plotter)

        # Spectral inversion
        try:
            sourcepar, sourcepar_err =\
                spectral_inversion(self.config, spec_st, weight_st)
        except (Exception, IndexError) as e:
            logging.info(e, exc_info=True)
            logger.info("Error in spectral_inversion")

        self.result = MagnitudeResult(sourcepar, sourcepar_err)

        if math.isnan(self.result.magnitude):
            logger.info("No magnitude value for %s.%s.%s.%s" % (
                self.network_code,
                self.station_code,
                self.location_code,
                self.channel_code,
            ))
            return

        if self.config.PLOT_SHOW or self.config.PLOT_SAVE:
            # Save output
            sourcepar_mean = \
                write_output(self.config, sourcepar, sourcepar_err)

            # Save residuals
            spectral_residuals(self.config, spec_st, sourcepar_mean)

            # Plotting
            nspec = len(set(s.id[:-1] for s in spec_st))
            ncols = 4 if nspec > 6 else 3
            plot_spectra(self.config, spec_st, specnoise_st,
                         plottype='regular', ncols=ncols,
                         async_plotter=plotter)
            plot_spectra(self.config, specnoise_st, plottype='noise',
                         ncols=ncols, async_plotter=plotter)
            plot_spectra(self.config, weight_st, plottype='weight',
                         ncols=ncols, async_plotter=plotter)

        logger.info("Magnitude for %s.%s.%s.%s: %s" % (
            self.network_code,
            self.station_code,
            self.location_code,
            self.channel_code,
            self.result.mws[0],
        ))
