#ifndef __AMPLITUDEPROCESSOR_MWS_H__
#define __AMPLITUDEPROCESSOR_MWS_H__

#include <boost/python.hpp>

#include <seiscomp3/core/plugin.h>
#include <seiscomp3/processing/amplitudeprocessor.h>
#include <seiscomp3/seismology/ttt.h>


#define AMP_TYPE "Mws"
#define INTERNAL_AMP_TYPE "Mwsi"


using namespace std;
using namespace Seiscomp;
using namespace Seiscomp::DataModel;
using namespace Seiscomp::Processing;


class AmplitudeProcessor_Mwsi : public AmplitudeProcessor {
    public:
        inline AmplitudeProcessor_Mwsi()
            : AmplitudeProcessor(INTERNAL_AMP_TYPE){};

        inline bool computeAmplitude(const DoubleArray &data,
                                     size_t i1, size_t i2,
                                     size_t si1, size_t si2,
                                     double offset,
                                     AmplitudeIndex *dt,
                                     AmplitudeValue *amplitude,
                                     double *period,
                                     double *snr){
            return true;
        };

        /**
         * Redefine the method to remove the gain.
         */
        bool deconvolveData(Response *resp, DoubleArray &data,
                            int numberOfIntegrations);

    friend class AmplitudeProcessor_Mws;
};


class AmplitudeProcessor_Mws : public AmplitudeProcessor {

    private:
        unsigned int _verbosity = 2;

        string _processorName;

        string _networkCode;
        string _stationCode;
        string _locationCode;
        string _channelCode;
        string _methodID;
        double _pickTimeBefore = 60;
        double _pickTotalTime = 180;
        string _filterStr;

        mutable AmplitudeProcessor_Mwsi _ampZ;
        mutable AmplitudeProcessor_Mwsi _ampN;
        mutable AmplitudeProcessor_Mwsi _ampE;
        bool _ampZReady = false;
        bool _ampNReady = false;
        bool _ampEReady = false;

        // Python namespace, used for the Python interpreter
        boost::python::object _pyNs;

        /**
         * Catch Python error.
         */
        string handle_pyerror();

        /**
         * Retrieve a Python string config.
         */
        void getPyString(
            const Settings& settings,
            const string& group,
            const string& param,
            const string& defaultValue);

        /**
         * Retrieve a Python double config.
         */
        void getPyDouble(
            const Settings& settings,
            const string& group,
            const string& param,
            const string& defaultValue);

        /**
         * Retrieve a Python boolean config.
         */
        void getPyBool(
            const Settings& settings,
            const string& group,
            const string& param,
            const string& defaultValue);

        /**
         * Init Python config.
         */
        void setupPython(const Settings& settings);

        /**
         * Send data to the Python wrapper to create a Trace.
         */
        void addTrace(Component component);

        /**
         * Return the earlier pick of the Origin for the given phase
         * code and station.
         */
        double getOriginPickTime(const Origin* origin, const string& phaseCode, bool firstLetterOnly);

        /**
         * Return the theoretical phase time.
         */
        double getTheoreticalPickTime(
            const Origin* origin,
            const Station* station,
            const string& phaseCode);

        /**
         * Return the real or theoretical pick time.
         */
        double getPPickTime(
            const Origin* origin,
            const Station* station);

        /**
         * Return the real or theoretical pick time.
         */
        double getSPickTime(
            const Origin* origin,
            const Station* station);

        /**
         * Send P and S picks to the Python wrapper.
         *
         * If the pick is not found on the origin, the theoretical pick
         * is returned.
         */
       void addPicks(const Origin* origin, const Station* station);

        /**
         * Update status of all processors (main + channel processors).
         */
       void updateStatus();

        void newAmplitude(const AmplitudeProcessor *proc,
                          const AmplitudeProcessor::Result &res);

    public:
        AmplitudeProcessor_Mws();

        void setConfig(const Config &config);

        const AmplitudeProcessor *componentProcessor(Component comp) const;

        const DoubleArray *processedData(Component comp) const;

        void reprocess(OPT(double) searchBegin, OPT(double) searchEnd);

        void reset();

        void computeTimeWindow();

        bool setup(const Settings& settings);

        void setTrigger(const Core::Time& trigger);

        double timeWindowLength(double distance) const;

        bool feed(const Record *record);

        inline bool computeAmplitude(const DoubleArray &data,
                                     size_t i1, size_t i2,
                                     size_t si1, size_t si2,
                                     double offset,
                                     AmplitudeIndex *dt,
                                     AmplitudeValue *amplitude,
                                     double *period,
                                     double *snr){
            return false;
        };
};

#endif
