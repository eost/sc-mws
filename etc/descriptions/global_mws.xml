<?xml version="1.0" encoding="UTF-8"?>
<seiscomp>
	<plugin name="Mws">
		<extends>global</extends>
		<description>
		Moment magnitude from the modelization of waveform spectra
		</description>
	</plugin>
	<binding name="Mws" module="global">
		<description>
		Moment magnitude from the modelization of waveform spectra
		</description>
		<configuration>
			<group name="Mws">
				<group name="general">
					<parameter name="PLOT_SHOW" type="boolean" default="false" />
					<parameter name="PLOT_SAVE" type="boolean" default="false" />
					<parameter name="PLOT_SAVE_FORMAT" type="string" default="png">
						<description>
						Options are 'png' or 'pdf'
						</description>
					</parameter>
					<parameter name="outdir" type="string" />
					<parameter name="database_file" type="string">
						<description>
						Database file for storing output parameters
						</description>
					</parameter>
					<parameter name="max_epi_dist" type="double">
						<description>
						Maximum epicentral distance (km) to process a trace
						</description>
					</parameter>
					<parameter name="pick_time_before" type="double" default="100" />
					<parameter name="pick_total_time" type="double" default="3600" />
					<parameter name="filter" type="string" default="">
						<description>
						Default filter used
						</description>
					</parameter>
				</group>
				<group name="initial">
					<parameter name="vp" type="double" default="5.5">
						<description>
						P wave velocity
						</description>
					</parameter>
					<parameter name="vs" type="double" default="3.2">
						<description>
						S wave velocity
						</description>
					</parameter>
					<parameter name="rho" type="double" default="2500">
						<description>
						Density
						</description>
					</parameter>
					<parameter name="rps" type="double" default="0.62">
						<description>
						Radiation pattern coefficient
						</description>
					</parameter>
					<parameter name="bsd" type="double" default="1000000">
						<description>
						Brune stress drop (MPa)
						</description>
					</parameter>
				</group>
				<group name="S/N">
					<parameter name="rmsmin" type="double" default="0">
						<description>
						Minimum rms (in trace units before instrument
						corrections) to consider a trace as noise
						</description>
					</parameter>
					<parameter name="vp_tt" type="double">
						<description>
						P wave velocity for travel time calculation (if None, a
						global velocity model is used instead)
						</description>
					</parameter>
					<parameter name="vs_tt" type="double">
						<description>
						S wave velocity for travel time calculation (if None, a
						global velocity model is used instead)
						</description>
					</parameter>
					<parameter name="sn_min" type="double" default="0">
						<description>
						S/N ratio min
						</description>
					</parameter>
					<parameter name="gap_max" type="double">
						<description>
						Maximum gap length for the whole trace, in seconds
						</description>
					</parameter>
					<parameter name="overlap_max" type="double">
						<description>
						Maximum overlap length for the whole trace, in seconds
						</description>
					</parameter>
				</group>
				<group name="spectrum">
					<parameter name="wave_type" type="string" default="S">
						<description>
						Options are 'S', 'SH' and 'SV'
						</description>
					</parameter>
					<parameter name="time_domain_int" type="boolean" default="false" />
					<parameter name="ignore_vertical" type="boolean" default="false" />
					<parameter name="pre_s_time" type="double" default="1">
						<description>
						S wave window, in seconds
						</description>
					</parameter>
					<parameter name="taper_halfwidth" type="double" default="0.05">
						<description>
						Taper between 0 (no taper) and 0.5
						</description>
					</parameter>
					<parameter name="spectral_win_length" type="double">
						<description>
						Spectral window length (sec). Signal is tapered, and
						then zero padded to this window length, so that the
						spectral sampling is fixed to 1 / spectral_win_length.
						Let unset to use S-wave window as spectral window
						length.
						</description>
					</parameter>
					<parameter name="bp_freqmin_acc" type="double" default="1">
						<description>
						Band-pass frequencies for accelerometers and
						velocimeters (Hz)
						</description>
					</parameter>
					<parameter name="bp_freqmax_acc" type="double" default="50">
						<description>
						Band-pass frequencies for accelerometers and
						velocimeters (Hz)
						</description>
					</parameter>
					<parameter name="bp_freqmin_shortp" type="double" default="1">
						<description>
						Band-pass frequencies for accelerometers and
						velocimeters (Hz)
						</description>
					</parameter>
					<parameter name="bp_freqmax_shortp" type="double" default="40">
						<description>
						Band-pass frequencies for accelerometers and
						velocimeters (Hz)
						</description>
					</parameter>
					<parameter name="bp_freqmin_broadb" type="double" default="0.5">
						<description>
						Band-pass frequencies for accelerometers and
						velocimeters (Hz)
						</description>
					</parameter>
					<parameter name="bp_freqmax_broadb" type="double" default="40">
						<description>
						Band-pass frequencies for accelerometers and
						velocimeters (Hz)
						</description>
					</parameter>
					<parameter name="freq1_acc" type="double" default="1">
						<description>
						Filter cut frequencies for accelerometers and
						velocimeters
						</description>
					</parameter>
					<parameter name="freq2_acc" type="double" default="30">
						<description>
						Filter cut frequencies for accelerometers and
						velocimeters
						</description>
					</parameter>
					<parameter name="freq1_shortp" type="double" default="1">
						<description>
						Filter cut frequencies for accelerometers and
						velocimeters
						</description>
					</parameter>
					<parameter name="freq2_shortp" type="double" default="30">
						<description>
						Filter cut frequencies for accelerometers and
						velocimeters
						</description>
					</parameter>
					<parameter name="freq1_broadb" type="double" default="0.5">
						<description>
						Filter cut frequencies for accelerometers and
						velocimeters
						</description>
					</parameter>
					<parameter name="freq2_broadb" type="double" default="30">
						<description>
						Filter cut frequencies for accelerometers and
						velocimeters
						</description>
					</parameter>
				</group>
				<group name="inversion">
					<parameter name="weighting" type="string" default="noise">
						<description>
						Weighting type. Options are 'noise', 'frequency' or
						'None'.
						</description>
					</parameter>
					<parameter name="f_weight" type="double" default="7">
						<description>
						Parameters for frequency weighting:
						weight for f &lt;= f_weight,
						1 for f &gt; f_weight
						</description>
					</parameter>
					<parameter name="weight" type="double" default="10">
					</parameter>
					<parameter name="norm_weight_integral_min" type="double">
					</parameter>
					<parameter name="spectral_sn_min" type="double" default="0">
						<description>
						Spectral S/N ratio min, below which a spectrum will be
						skipped
						</description>
					</parameter>
					<parameter name="spectral_sn_freq_min" type="double">
						<description>
						Min frequency to compute spectral S/N ratio
						</description>
					</parameter>
					<parameter name="spectral_sn_freq_max" type="double">
						<description>
						Max frequency to compute spectral S/N ratio
						</description>
					</parameter>
					<parameter name="fc_0" type="double">
						<description>
						Initial values for fc
						</description>
					</parameter>
					<parameter name="t_star_0" type="double" default="0.045">
						<description>
						Initial value for t_star
						</description>
					</parameter>
					<parameter name="invert_t_star_0" type="boolean" default="false">
						<description>
						Try to invert for t_star_0. If inverted t_star_0 is
						non-positive, then fixed t_star_0 will be used.
						</description>
					</parameter>
					<parameter name="t_star_0_variability" type="double" default="0.1">
						<description>
						Allowed variability around inverted t_star_0 in the
						main inversion (expressed as a fraction of t_star_0,
						between 0 and 1). If inverted t_star_0 is non-positive,
						then t_star_min_max is used.
						</description>
					</parameter>
					<parameter name="inv_algorithm" type="string" default="LM">
						<description>
						Inversion algorithm. Options are 'LM' (
						Levenberg-Marquardt algorithm (warning: Trust Region
						Reflective algorithm will be used instead if bounds are
						provided), 'TNC' (truncated Newton algorithm (with
						bounds)) and 'BH'.
						</description>
					</parameter>
					<parameter name="fc_min" type="double" />
					<parameter name="fc_max" type="double" />
					<parameter name="t_star_min" type="double">
						<description>
						t_star_min_max does not superseed t_star_0_variability
						</description>
					</parameter>
					<parameter name="t_star_max" type="double">
						<description>
						t_star_min_max does not superseed t_star_0_variability
						</description>
					</parameter>
					<parameter name="Qo_min" type="double">
						<description>
						Qo bounds (converted into t_star bounds in the code)
						</description>
					</parameter>
					<parameter name="Qo_max" type="double">
						<description>
						Qo bounds (converted into t_star bounds in the code)
						</description>
					</parameter>
				</group>
				<group name="radiated_energy">
					<parameter name="max_freq_Er" type="double">
						<description>
						Maximum frequency (Hz) to measure radiated energy Er
						(above this frequency, the finite-band correction of
						Di Bona &amp; Rovelli, 1988, will be applied)
						</description>
					</parameter>
				</group>
				<group name="post_processing">
					<parameter name="min_corner_freq" type="double" default="0.5">
						<description>
						Min acceptable corner frequencies (Hz)
						</description>
					</parameter>
					<parameter name="max_corner_freq" type="double" default="40">
						<description>
						Max acceptable corner frequencies (Hz)
						</description>
					</parameter>
				</group>
			</group>
		</configuration>
	</binding>
</seiscomp>
